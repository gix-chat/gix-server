#pragma once

#include <cmath>
#include <map>
#include <string>
#include <utility>
#include <vector>

namespace gix {
namespace algorithm {

template <typename NumericT>
NumericT minimum(NumericT&& a, NumericT&& b, NumericT&& c)
{
    if (a < b) {
        if (a < c) {
            return a;
        } else {
            return c;
        }
    } else {
        if (b < c) {
            return b;
        } else {
            return c;
        }
    }
}

double euclideanDistance(const std::pair<double, double>& from, const std::pair<double, double>& to)
{
    return std::sqrt(std::pow(from.first - to.first, 2) + std::pow(from.second - to.second, 2));
}

double keyDistanceOnKeyboard(const char src, const char dst)
{
    static std::map<char, std::pair<double, double> > keyToCoordinate = {
        { '`',  { 0.0,      4.0 } },
        { '~',  { 0.0,      4.0 } },
        { '1',  { 1.0,      4.0 } },
        { '!',  { 1.0,      4.0 } },
        { '2',  { 2.0,      4.0 } },
        { '@',  { 2.0,      4.0 } },
        { '3',  { 3.0,      4.0 } },
        { '#',  { 3.0,      4.0 } },
        { '4',  { 4.0,      4.0 } },
        { '$',  { 4.0,      4.0 } },
        { '5',  { 5.0,      4.0 } },
        { '%',  { 5.0,      4.0 } },
        { '6',  { 6.0,      4.0 } },
        { '^',  { 6.0,      4.0 } },
        { '7',  { 7.0,      4.0 } },
        { '&',  { 7.0,      4.0 } },
        { '8',  { 8.0,      4.0 } },
        { '*',  { 8.0,      4.0 } },
        { '9',  { 9.0,      4.0 } },
        { '(',  { 9.0,      4.0 } },
        { '0',  { 10.0,     4.0 } },
        { ')',  { 10.0,     4.0 } },
        { '-',  { 11.0,     4.0 } },
        { '_',  { 11.0,     4.0 } },
        { '=',  { 12.0,     4.0 } },
        { '+',  { 12.0,     4.0 } },
        { 'q',  { 1.5,      3.0 } },
        { 'Q',  { 1.5,      3.0 } },
        { 'w',  { 2.5,      3.0 } },
        { 'W',  { 2.5,      3.0 } },
        { 'e',  { 3.5,      3.0 } },
        { 'E',  { 3.5,      3.0 } },
        { 'r',  { 4.5,      3.0 } },
        { 'R',  { 4.5,      3.0 } },
        { 't',  { 5.5,      3.0 } },
        { 'T',  { 5.5,      3.0 } },
        { 'y',  { 6.5,      3.0 } },
        { 'Y',  { 6.5,      3.0 } },
        { 'u',  { 7.5,      3.0 } },
        { 'U',  { 7.5,      3.0 } },
        { 'i',  { 8.5,      3.0 } },
        { 'I',  { 8.5,      3.0 } },
        { 'o',  { 9.5,      3.0 } },
        { 'O',  { 9.5,      3.0 } },
        { 'p',  { 10.5,     3.0 } },
        { 'P',  { 10.5,     3.0 } },
        { '[',  { 11.5,     3.0 } },
        { '{',  { 11.5,     3.0 } },
        { ']',  { 12.5,     3.0 } },
        { '}',  { 12.5,     3.0 } },
        { '\\', { 13.5,     3.0 } },
        { '|',  { 13.5,     3.0 } },
        { 'a',  { 1.75,     2.0 } },
        { 'A',  { 1.75,     2.0 } },
        { 's',  { 2.75,     2.0 } },
        { 'S',  { 2.75,     2.0 } },
        { 'd',  { 3.75,     2.0 } },
        { 'D',  { 3.75,     2.0 } },
        { 'f',  { 4.75,     2.0 } },
        { 'F',  { 4.75,     2.0 } },
        { 'g',  { 5.75,     2.0 } },
        { 'G',  { 5.75,     2.0 } },
        { 'h',  { 6.75,     2.0 } },
        { 'H',  { 6.75,     2.0 } },
        { 'j',  { 7.75,     2.0 } },
        { 'J',  { 7.75,     2.0 } },
        { 'k',  { 8.75,     2.0 } },
        { 'K',  { 8.75,     2.0 } },
        { 'l',  { 9.75,     2.0 } },
        { 'L',  { 9.75,     2.0 } },
        { ';',  { 10.75,    2.0 } },
        { ':',  { 10.75,    2.0 } },
        { '\'', { 11.75,    2.0 } },
        { '\"', { 11.75,    2.0 } },
        { 'z',  { 2.25,     1.0 } },
        { 'Z',  { 2.25,     1.0 } },
        { 'x',  { 3.25,     1.0 } },
        { 'X',  { 3.25,     1.0 } },
        { 'c',  { 4.25,     1.0 } },
        { 'C',  { 4.25,     1.0 } },
        { 'v',  { 5.25,     1.0 } },
        { 'V',  { 5.25,     1.0 } },
        { 'b',  { 6.25,     1.0 } },
        { 'B',  { 6.25,     1.0 } },
        { 'n',  { 7.25,     1.0 } },
        { 'N',  { 7.25,     1.0 } },
        { 'm',  { 8.25,     1.0 } },
        { 'M',  { 8.25,     1.0 } },
        { ',',  { 9.25,     1.0 } },
        { '<',  { 9.25,     1.0 } },
        { '.',  { 10.25,    1.0 } },
        { '>',  { 10.25,    1.0 } },
        { '/',  { 11.25,    1.0 } },
        { '?',  { 11.25,    1.0 } },
    };

    return euclideanDistance(keyToCoordinate[src], keyToCoordinate[dst]);
}

/*
 * This function returns minimum edit distance from 'src' to 'dst'.
 * Algorithm is based on Levenshtein distance.
 */
int minEditDistance(const std::string& src, const std::string& dst)
{
    const int iMax = src.size() + 1;
    const int jMax = dst.size() + 1;
    using Row = std::vector<int>;
    using Matrix = std::vector<Row>;
    Matrix mtx(iMax, Row(jMax, 0));
    for (int i = 0; i < iMax; ++i) {
        mtx[i][0] = i;
    }
    for (int j = 0; j < jMax; ++j) {
        mtx[0][j] = j;
    }
    for (int i = 1; i < iMax; ++i) {
        for (int j = 1; j < jMax; ++j) {
            mtx[i][j] = minimum(mtx[i - 1][j] + 1,
                                mtx[i][j - 1] + 1,
                                mtx[i - 1][j - 1] + static_cast<int>(2 * keyDistanceOnKeyboard(src[i - 1], dst[j - 1])));
        }
    }
    return mtx[iMax - 1][jMax - 1];
}

/*
 * This function checks whether the 'sub' is a substring of the 'str'.
 */
bool isSubString(const std::string& sub, const std::string& dst)
{
    // TODO!
    return false;
}

} // namespace algorithm
} // namespace gix
