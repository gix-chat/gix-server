#include <generic/EventManager.h>

using namespace gix::generic;

SINGLETON_DECL(EventManager)

void EventManager::startEventLoop()
{
    while (true) {
        m_mutex.lock();
        if (m_eventLoop.isEmpty()) {
            m_mutex.unlock();
            return;
        }
        auto event = m_eventLoop.front();
        m_eventLoop.pop();
        m_mutex.unlock();
        event();
    }
}
