#pragma once

#include <functional>
#include <mutex>
#include <queue>

#include <utility/Macros.h>
#include <utility/SingletonBase.h>

namespace gix
{
namespace generic
{

class EventManager SINGLETON_BASE(std::recursive_mutex)
{
    SINGLETON_DEF(EventManager)

public:
    template <typename FuncT>
    void addEvent(FuncT functor);

private:
    void startEventLoop();

private:
    std::queue<std::function<void ()> > m_eventLoop;
};

} // namespace generic
} // namespace gix

#include <generic/EventManager.hpp>
