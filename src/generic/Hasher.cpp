#include <generic/Hasher.h>
#include <generic/MD5.h>

using namespace gix::generic;

std::string Hasher::md5(const std::string& str)
{
    MD5 md5 = MD5();
    md5.update(str.c_str(), str.size());
    md5.finalize();
    return md5.toString();
}

Hash128 Hasher::md5Shortened(const std::string& str)
{
    MD5 md5 = MD5();
    md5.update(str.c_str(), str.size());
    md5.finalize();
    return md5.shortened();
}
