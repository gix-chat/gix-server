#pragma once

#include <string>

namespace gix {
namespace generic {

using Hash128 = std::basic_string<unsigned char>;

class Hasher
{
public:
    ///@brief hashing mechanism which returns 32 bit string
    static std::string md5(const std::string& str);

    ///@brief hashing mechanism which returns 16 bit basic string
    static Hash128 md5Shortened(const std::string& str);
};

} //namespace generic
} //namespace gix
