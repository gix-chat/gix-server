#include <server/SignalHandler.h>
#include <server/Command.h>
#include <server/Parser.h>

#include <test/unit_tests.h>

namespace {
    const std::string whitespace = std::string(" \t");
}

int main()
{
    gix::server::setSignalHandlers();
    const auto pars = gix::parser::Parser::get_instance();

#ifdef TEST
    testUserData();
    testActionSerialization();
    testActionsManager();
    testUserManager();
#endif

    auto proc = CommandProcess::get_instance();
    for (std::string line; std::cout << "> " && std::getline(std::cin, line);) {
        if (!line.empty()) {
            pars->setValue(line);
            size_t fPos = line.find_first_not_of(whitespace, 0);
            proc->run(line.substr(fPos, line.find_first_of(whitespace, fPos) - fPos)); 
        }
    }
}
