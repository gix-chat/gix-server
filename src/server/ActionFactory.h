#include <server/Actions.h>

namespace gix {
namespace server {

class ActionFactory
{
public:
    static ActionBase* createAction(Action act);
};

ActionBase* ActionFactory::createAction(Action act)
{
    switch(act) {
        case Action::SendPersonalMessage:
            return new SendPersonalMessageAction();
        case Action::SendGroupMessage:
            return new SendGroupMessageAction();
        case Action::JoinGroup:
            return new JoinGroupAction();
        case Action::JoinGroupAccepted:
            return new JoinGroupAcceptedAction();
        case Action::LeftGroup:
            return new LeftGroupAction();
        case Action::RemoveFromGroup:
            return new RemoveFromGroupAction();
        default:
            return nullptr;
            break;
    }
    return nullptr;
}


} //namespace server
} //namespace gix

