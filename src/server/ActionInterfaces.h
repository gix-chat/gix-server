#pragma once

#include <string>

struct UserAssociated
{
    virtual bool isUserAssociated(const std::string& username) const = 0;
};

struct GroupAssociated
{
    virtual bool isGroupAssociated(const std::string& group) const = 0;
};
