#include <server/GlobalSerializers.h>

#include <server/Actions.h>

using namespace gix::server;

ByteArray SendPersonalMessageAction::serialize() const
{
    return toByteArray(static_cast<uint32_t>(Action::SendPersonalMessage))
         + toByteArray(m_senderUsername)
         + toByteArray(m_receiverUsername)
         + toByteArray(m_message);
}

int32_t SendPersonalMessageAction::deserialize(const Byte* data)
{
    auto result = 0;
    result += fromByteArray(data + result, m_senderUsername);
    result += fromByteArray(data + result, m_receiverUsername);
    result += fromByteArray(data + result, m_message);
    return result;
}

bool SendPersonalMessageAction::execute()
{
    // try to send the message to the receiver, if the letter is connected
    return true;
}

bool SendPersonalMessageAction::isUserAssociated(const std::string& username) const
{
    return m_receiverUsername == username;
}

ByteArray SendGroupMessageAction::serialize() const
{
    return toByteArray(static_cast<uint32_t>(Action::SendGroupMessage))
         + toByteArray(m_senderUsername)
         + toByteArray(m_groupName)
         + toByteArray(m_message)
         + toByteArray(m_receiversLeft);
}

int32_t SendGroupMessageAction::deserialize(const Byte* data)
{
    auto result = 0;
    result += fromByteArray(data + result, m_senderUsername);
    result += fromByteArray(data + result, m_groupName);
    result += fromByteArray(data + result, m_message);
    result += fromByteArray(data + result, m_receiversLeft);
    return result;
}

bool SendGroupMessageAction::execute()
{
    // try to send the message to all other participants in the group
    return m_receiversLeft.empty();
}

bool SendGroupMessageAction::isUserAssociated(const std::string& username) const
{
    return m_receiversLeft.find(username) != m_receiversLeft.end();
}

bool SendGroupMessageAction::isGroupAssociated(const std::string& group) const
{
    return m_groupName == group;
}

ByteArray JoinGroupAction::serialize() const
{
    return toByteArray(static_cast<uint32_t>(Action::JoinGroup))
         + toByteArray(m_joiningUsername)
         + toByteArray(m_groupOwner)
         + toByteArray(m_groupName);
}

int32_t JoinGroupAction::deserialize(const Byte* data)
{
    auto result = 0;
    result += fromByteArray(data + result, m_joiningUsername);
    result += fromByteArray(data + result, m_groupOwner);
    result += fromByteArray(data + result, m_groupName);
    return result;
}

bool JoinGroupAction::execute()
{
    // Try to send notification to group owner
    return true;
}

bool JoinGroupAction::isUserAssociated(const std::string& username) const
{
    return m_groupOwner == username;
}

bool JoinGroupAction::isGroupAssociated(const std::string& group) const
{
    return m_groupName == group;
}


ByteArray JoinGroupAcceptedAction::serialize() const
{
    return toByteArray(static_cast<uint32_t>(Action::JoinGroupAccepted))
         + toByteArray(m_groupName)
         + toByteArray(m_receiversLeft);
}

int32_t JoinGroupAcceptedAction::deserialize(const Byte* data)
{
    auto result = 0;
    result += fromByteArray(data + result, m_groupName);
    result += fromByteArray(data + result, m_receiversLeft);
    return result;
}

bool JoinGroupAcceptedAction::execute()
{
    // Try to send notification to the joining user
    return m_receiversLeft.empty();
}

bool JoinGroupAcceptedAction::isUserAssociated(const std::string& username) const
{
    return m_receiversLeft.find(username) != m_receiversLeft.end();
}

bool JoinGroupAcceptedAction::isGroupAssociated(const std::string& group) const
{
    return m_groupName == group;
}

ByteArray LeftGroupAction::serialize() const
{
    return toByteArray(static_cast<uint32_t>(Action::LeftGroup))
         + toByteArray(m_groupName)
         + toByteArray(m_receiversLeft);
}

int32_t LeftGroupAction::deserialize(const Byte* data)
{
    auto result = 0;
    result += fromByteArray(data + result, m_groupName);
    result += fromByteArray(data + result, m_receiversLeft);
    return result;
}

bool LeftGroupAction::execute()
{
    // Notify everyone in group
    return m_receiversLeft.empty();
}

bool LeftGroupAction::isGroupAssociated(const std::string& group) const
{
    return m_groupName == group;
}

bool LeftGroupAction::isUserAssociated(const std::string& username) const
{
    return m_receiversLeft.find(username) != m_receiversLeft.end();
}

ByteArray RemoveFromGroupAction::serialize() const
{
    return toByteArray(static_cast<uint32_t>(Action::RemoveFromGroup))
         + toByteArray(m_removedUsername)
         + toByteArray(m_groupName)
         + toByteArray(m_receiversLeft);
}

int32_t RemoveFromGroupAction::deserialize(const Byte* data)
{
    auto result = 0;
    result += fromByteArray(data + result, m_removedUsername);
    result += fromByteArray(data + result, m_groupName);
    result += fromByteArray(data + result, m_receiversLeft);
    return result;
}

bool RemoveFromGroupAction::execute()
{
    // Notify everyone in group
    auto isRemovedUserAware = true;
    return m_receiversLeft.empty() && isRemovedUserAware;
}

bool RemoveFromGroupAction::isGroupAssociated(const std::string& group) const
{
    return m_groupName == group;
}

bool RemoveFromGroupAction::isUserAssociated(const std::string& username) const
{
    return (username == m_removedUsername) ||
           (m_receiversLeft.find(username) != m_receiversLeft.end());
}
