#pragma once

#include <string>

namespace gix {
namespace server {

using Byte = unsigned char;
using ByteArray = ::std::basic_string<Byte>;

struct ActionBase
{
public:
    virtual ByteArray serialize() const = 0;
    virtual int deserialize(const Byte*) = 0;
    virtual bool execute() = 0;
    virtual ActionBase* clone() const = 0;
    virtual ~ActionBase() {}
};

enum class Action : uint32_t
{
    SendPersonalMessage,
    SendGroupMessage,
    JoinGroup,
    JoinGroupAccepted,
    LeftGroup,
    RemoveFromGroup
};

inline bool operator== (Action lhs, Action rhs)
{
    return static_cast<uint32_t>(lhs) == static_cast<uint32_t>(rhs);
}

} // namespace server
} // namespace gix

#include "ActionsDerived.hpp"
