#include <set>

#include <server/ActionInterfaces.h>
#include <utility/Macros.h>

using namespace gix::server;

struct SendPersonalMessageAction : public ActionBase
                                 , public UserAssociated
{
    ACTION_BASE_DECL
    USER_ASSOCIATED_DECL
    DEFAULT_CLONE_IMPL(SendPersonalMessageAction)

    std::string m_senderUsername;
    std::string m_receiverUsername;
    std::string m_message;
};

struct SendGroupMessageAction : public ActionBase
                              , public UserAssociated
                              , public GroupAssociated
{
    ACTION_BASE_DECL
    USER_ASSOCIATED_DECL
    GROUP_ASSOCIATED_DECL
    DEFAULT_CLONE_IMPL(SendGroupMessageAction)

    std::string             m_senderUsername;
    std::string             m_groupName;
    std::string             m_message;
    std::set<std::string>   m_receiversLeft;
};

struct JoinGroupAction : public ActionBase
                       , public GroupAssociated
                       , public UserAssociated
{
    ACTION_BASE_DECL
    GROUP_ASSOCIATED_DECL
    USER_ASSOCIATED_DECL
    DEFAULT_CLONE_IMPL(JoinGroupAction)

    std::string m_joiningUsername;
    std::string m_groupOwner;
    std::string m_groupName;
};

struct JoinGroupAcceptedAction : public ActionBase
                               , public GroupAssociated
                               , public UserAssociated
{
    ACTION_BASE_DECL
    GROUP_ASSOCIATED_DECL
    USER_ASSOCIATED_DECL
    DEFAULT_CLONE_IMPL(JoinGroupAcceptedAction)

    std::string             m_groupName;
    std::set<std::string>   m_receiversLeft;
};

struct LeftGroupAction : public ActionBase
                       , public GroupAssociated
                       , public UserAssociated
{
    ACTION_BASE_DECL
    GROUP_ASSOCIATED_DECL
    USER_ASSOCIATED_DECL
    DEFAULT_CLONE_IMPL(LeftGroupAction)

    std::string             m_groupName;
    std::set<std::string>   m_receiversLeft;
};

struct RemoveFromGroupAction : public ActionBase
                             , public GroupAssociated
                             , public UserAssociated
{
    ACTION_BASE_DECL
    GROUP_ASSOCIATED_DECL
    USER_ASSOCIATED_DECL
    DEFAULT_CLONE_IMPL(RemoveFromGroupAction)

    std::string             m_removedUsername;
    std::string             m_groupName;
    std::set<std::string>   m_receiversLeft;
};
