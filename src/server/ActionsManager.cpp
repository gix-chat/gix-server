#include <mutex>

#include <server/ActionFactory.h>
#include <server/ActionsManager.h>
#include <server/ConfigurationManager.h>
#include <server/FileLoader.h>

using namespace gix::server;

SINGLETON_DEF(ActionsManager)

ActionsManager::ActionsManager()
    : m_isCurrentStateSaved(true)
{
}

ActionsManager::~ActionsManager()
{
    save();
    for (auto p : m_allActions) {
        delete p;
        p = nullptr;
    }
    m_allActions.clear();
}

void ActionsManager::save() const
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    if (m_isCurrentStateSaved) {
        return;
    }
    m_isCurrentStateSaved = true;
    const auto mgr = ConfigurationManager::get_instance();
    FileLoader outFileLoader(mgr->getActionPath(), std::ios::out);
    outFileLoader.load();
    for (const auto& iter : m_allActions) {
        outFileLoader.write(iter->serialize());
    }
}

void ActionsManager::load()
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    m_isCurrentStateSaved = false;
    const auto mgr = ConfigurationManager::get_instance();
    FileLoader::Data data;
    {
        FileLoader inputFileLoader(mgr->getActionPath(), std::ios::in);
        inputFileLoader.load();
        data = inputFileLoader.read();
    }
    uint32_t pos = 0;
    constexpr uint8_t typeInfoSize = sizeof(uint32_t);
    while (pos < data.size()) {
        auto actObj = ActionFactory::createAction(static_cast<Action>(*reinterpret_cast<uint32_t*>(&(data[pos]))));
        pos += typeInfoSize;
        pos += actObj->deserialize(&(data[pos]));
        addAction(actObj);
    }
}

void ActionsManager::addAction(ActionBase* act)
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    m_isCurrentStateSaved = false;
    m_allActions.insert(act);
}

void ActionsManager::removeAction(ActionBase* act)
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    m_isCurrentStateSaved = false;
    auto iter = m_allActions.find(act);
    if (iter != m_allActions.end()) {
        m_allActions.erase(iter);
    }
}

void ActionsManager::removeActionsAssociatedWithUser(const std::string& user)
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    // TODO!
}

void ActionsManager::removeActionsAssociatedWithGroup(const std::string& group)
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    // TODO!
}

void ActionsManager::executeActionsAssociatedWithUser(const std::string& user)
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    // TODO!
}

void ActionsManager::executeActionsAssociatedWithGroup(const std::string& group)
{
    std::lock_guard<RecursiveSpin> lock(m_mutex);
    // TODO!
}
