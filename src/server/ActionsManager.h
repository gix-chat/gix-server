#pragma once

#include <unordered_set>

#include <server/Actions.h>
#include <utility/Macros.h>
#include <utility/SingletonBase.h>
#include <utility/Spin.h>

//#include "ActionContainer.h"

//typedef std::vector<ActionBase*> ActionsContainer;
template <typename T>
using ActionsContainer = std::unordered_set<T>;

namespace gix {
namespace server {

class ActionsManager SINGLETON_BASE(RecursiveSpin)
{
    SINGLETON_DECL(ActionsManager)

public:
    void save() const;
    void load();
    void addAction(ActionBase* act);
    void removeAction(ActionBase* act);
    void removeActionsAssociatedWithUser(const std::string& user);
    void removeActionsAssociatedWithGroup(const std::string& group);
    void executeActionsAssociatedWithUser(const std::string& user);
    void executeActionsAssociatedWithGroup(const std::string& group);

    inline const ActionsContainer<ActionBase*>& getActions() const
    {
        return m_allActions;
    }

private:
    ActionsContainer<ActionBase*>       m_allActions;
    mutable bool                        m_isCurrentStateSaved;
};

} //
} //namespace gix

