#include <server/Argument.h>
#include <server/Parser.h>

Argument::Argument(const std::string& name, const std::string& value, unsigned index)
    : m_name(name)
    , m_value(value)
    , m_index(index)
{
}

const std::string& Argument::getName() const
{
    return m_name;
}

const std::string& Argument::getValue()
{
    m_value = gix::parser::Parser::get_instance()->getCurrentValue(m_name);
    return m_value;
}

void Argument::setValue(const std::string& value)
{
    m_value = value;
}

const unsigned Argument::getIndex() const
{
    return m_index;
}

void Argument::setIndex(unsigned idx)
{
    m_index = idx;
}
