#pragma once

#include <string>

class Argument
{
public:
    Argument(const std::string& name = std::string(),
            const std::string& value = std::string(), unsigned index = 0);
            
    const std::string& getName() const;

    const std::string& getValue();

    void setValue(const std::string& value);

    const unsigned getIndex() const;

    void setIndex(unsigned idx);

private:
    std::string m_name;
    std::string m_value;
    unsigned m_index;
};
