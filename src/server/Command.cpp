#include <iostream>
#include <string>
#include <limits>
#include <mutex>

#include <algorithm/Algorithm.h>
#include <server/Command.h>
#include <server/ConfigurationManager.h>
#include <server/SignalHandler.h>

// we sould write package which will register all commands
namespace {

    void RegisterCommands(CommandProcess& proc)
    {
        proc.addCommand(new ExitCommand());
        proc.addCommand(new GetCommand());
        proc.addCommand(new SetCommand());
        proc.addCommand(new HelpCommand());
    }
}

Command::Command(const std::string& name, const std::string& description,
                Category category, Type type)
        : m_name(name)
        , m_description(description)
        , m_category(category)
        , m_type(type)
{
}

Command::~Command()
{
    for (auto& iter : m_arguments) {
        delete iter;
    }
}

const std::string& Command::getName() const
{
    return m_name;
}

const std::string& Command::getDescription() const
{
    return m_description;
}

Type Command::getType() const
{
    return m_type;
}

Category Command::getCategory() const
{
    return m_category;
}

void Command::addArgument(Argument* arg)
{
    for (const auto& iter : m_arguments) {
        if (iter->getName() == arg->getName()) {
            throw("The argument ialready registerd");
        }
    }
    arg->setIndex(m_arguments.size());
    m_arguments.emplace_back(arg);
}

HelpCommand::HelpCommand()
    : Command("help",
            "shows the specified command help",
            Category::COMAND_LINE,
            Type::REPORT)
{
    m_commandName = new Argument("help");
    addArgument(m_commandName);
}

bool HelpCommand::execute()
{
    if (m_commandName->getValue().empty()) {
        std::cout << getDescription() << std::endl;
        return true;
    }
    Command* cmd = CommandProcess::get_instance()->getCommand(m_commandName->getValue());
    if (cmd != nullptr) {
        std::cout << cmd->getDescription() << std::endl;;
        return true;
    }
    return false;
}

ExitCommand::ExitCommand()
    : Command("exit",
            "if group is specified quits specified group, otherwase whole groups",
            Category::COMAND_LINE,
            Type::REMOVE)
    , m_group()
{
    m_group = new Argument("-group");
    addArgument(m_group);
}

bool ExitCommand::execute()
{
    if (m_group->getValue().empty()) {
        gix::server::normalExit();
    } else {
        //TODO exit specified group
    }
    return true;
}

GetCommand::GetCommand()
    : Command("get_config",
            "gets the specified config properties othewise whole configs properties",
            Category::COMAND_LINE,
            Type::REPORT)
{
    m_configName = new Argument("-config");
    addArgument(m_configName);
}

bool GetCommand::execute()
{
     return gix::server::ConfigurationManager::get_instance()->report(m_configName->getValue());
}

SetCommand::SetCommand()
    : Command("set_config",
            "sets the value to specified config",
            Category::COMAND_LINE,
            Type::MODIFY)
{
    m_configName = new Argument("-config");
    addArgument(m_configName);
    m_value = new Argument("-value");
    addArgument(m_value);
}

bool SetCommand::execute()
{
    if (m_configName->getValue().empty()) {
        std::cout << "ERROR config is not specified" << std::endl;
        return false;
    }
    return gix::server::ConfigurationManager::get_instance()->set(m_configName->getValue(), m_value->getValue());
}

SINGLETON_DEF(CommandProcess)

CommandProcess::CommandProcess()
{
    RegisterCommands(*this);
}

CommandProcess::~CommandProcess()
{
    for (auto iter : m_commands) {
        delete iter;
    }
}

Command* CommandProcess::getCommand(const std::string& cmdName) const
{
    for (const auto iter : m_commands) {
        if (cmdName == iter->getName()) {
            return iter;
        }
    }
    const auto candidates = getClosestCandidates(cmdName);
    std::cerr << "The command \'" << cmdName << "\' is not registered" << std::endl;
    std::cerr << "Maybe you meant ";
    for (const auto& cand : candidates) {
        std::cerr << '\'' << cand << "\' ";
    }
    std::cerr << std::endl;
    return nullptr;
}

void CommandProcess::addCommand(Command* cmd)
{
    std::lock_guard<Spin> lock(m_mutex);
    m_commands.emplace_back(cmd);
}

void CommandProcess::run(const std::string& str) const
{
    Command* cmd = getCommand(str);
    if (cmd == nullptr) {
        return;
    }
    cmd->execute();
}

std::vector<std::string> CommandProcess::getClosestCandidates(const std::string& cmdName) const
{
    int currentDist = std::numeric_limits<int>::max();
    std::vector<std::string> result;
    for (const auto& cmd : m_commands) {
        const auto currentCmdName = cmd->getName();
        if (gix::algorithm::isSubString(cmdName, currentCmdName)) {
            result.push_back(cmd->getName());
            continue;
        }
        auto dist = gix::algorithm::minEditDistance(cmdName, currentCmdName);
        if (dist <= currentDist) {
            if (dist < currentDist) {
                result.clear();
                currentDist = dist;
            }
            result.push_back(cmd->getName());
        }
    }
    return result;
}
