#pragma once

#include <vector>

#include <server/Argument.h>
#include <utility/Macros.h>
#include <utility/SingletonBase.h>
#include <utility/Spin.h>

/**
 * @brief enum to deferentiate commands by there action types
 */
enum class Type : unsigned char
{
    MODIFY,
    ACCESS,
    REMOVE,
    UPDATE,
    REPORT
};

/**
 *  @brief this enum is designed to categorise commands
 *         \this enum will be needed for the further development
 */
enum class Category : unsigned char
{
    CUSTOM       = 0x1,
    SERVER       = 0x2,
    CLIENT       = 0x4,
    COMAND_LINE  = 0x8
};

class Command
{
public:
    Command(const std::string& name, const std::string& description,
            Category category = Category::COMAND_LINE, Type type = Type::UPDATE);

    virtual ~Command();

    virtual bool execute() = 0;
    virtual const std::string& getName() const;
    virtual const std::string& getDescription() const;
    virtual Type getType() const;
    virtual Category getCategory() const;

    void addArgument(Argument* arg);

private:
    std::string m_name;
    std::string m_description;
    Category m_category;
    Type m_type;

    std::vector<Argument*> m_arguments;
};

class HelpCommand : public Command
{
public:
    HelpCommand();

    virtual bool execute() override;

private:
    Argument* m_commandName;
};


class ExitCommand : public Command
{
public:
    ExitCommand();

    virtual bool execute() override;

private:
    Argument* m_group;
};

class GetCommand : public Command
{
public:
    GetCommand();

    virtual bool execute() override;

private:
    Argument* m_configName;
};

class SetCommand : public Command
{
public:
    SetCommand();

    virtual bool execute() override;

private:
    Argument* m_configName;
    Argument* m_value;
};

class CommandProcess SINGLETON_BASE(Spin)
{
    SINGLETON_DECL(CommandProcess)

public:
    Command* getCommand(const std::string& cmdName) const;
    void addCommand(Command* cmd);
    void run(const std::string& str) const;

private:
    std::vector<std::string> getClosestCandidates(const std::string& cmdName) const;
    
private:
    std::vector<Command*> m_commands;
};
