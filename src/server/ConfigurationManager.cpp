#include <mutex>
#include <fstream>

#include <server/ConfigurationManager.h>

using namespace gix::server;

SINGLETON_DEF(ConfigurationManager)

ConfigurationManager::ConfigurationManager()
    : m_configPath("configFile")
{
    std::lock_guard<Spin> lock(m_mutex);
    std::ifstream infile(m_configPath, std::ios::in);
    if (!infile.good()) {
        m_port = 0;
        m_userDataPath = "userData";
        m_actionPath = "actions";
    }
    infile >> m_port;
    std::string dummy;
    std::getline(infile, dummy);
    std::getline(infile, m_userDataPath);
    std::getline(infile, m_actionPath);
    std::getline(infile, m_usersGroupsPath);
}

ConfigurationManager::~ConfigurationManager()
{
    std::lock_guard<Spin> lock(m_mutex);
    std::ofstream outfile(m_configPath, std::ios::out);
    if (!outfile.good()) {
        return;
    }
    outfile << m_port << std::endl;
    outfile << m_userDataPath << std::endl;
    outfile << m_actionPath << std::endl;
    outfile << m_usersGroupsPath << std::endl;
}

bool ConfigurationManager::report(const std::string& str) const
{
    if (str.empty()) {
        std::cout << "Port:        " << m_port << std::endl;
        std::cout << "config_path: " << m_configPath << std::endl;
        std::cout << "user_data:   " << m_userDataPath << std::endl; 
        std::cout << "action_path: " << m_actionPath << std::endl;
        std::cout << "group_path:  " << m_usersGroupsPath << std::endl;
        return true;
    }
    if (str == "port") {
        std::cout << "Port: " << m_port << std::endl;
        return true;
    } else if (str == "config_path") {
        std::cout << "config_path: " << m_configPath << std::endl;
        return true;
    } else if (str == "user_data") {
        std::cout << "user_data: " << m_userDataPath << std::endl; 
        return true;
    } else if (str == "action_path") {
        std::cout << "action_path: " << m_actionPath << std::endl;
        return true;
    } else if (str == "group_path") {
        std::cout << "group_path: " << m_usersGroupsPath << std::endl;
        return true;
    } else {
        std::cerr << "ERROR the specified " << str << " config is unknown" << std::endl;
        return false;
    }
    return false;
}

bool ConfigurationManager::set(const std::string& str, const std::string& val)
{
    std::lock_guard<Spin> lock(m_mutex);
    if (str == "port") {
        try {
            m_port = std::stoi(val);
        } catch (const std::invalid_argument& ex) {
            std::cerr << "ERROR the specifid value " << val << " is invalid try again" << std::endl;
            return true;
        }
    } else if (str == "config_path") {
        std::cout << "INFO config path is not changable" << std::endl;
        return false;
    } else if (str == "user_data") {
        m_userDataPath = val;
        return true;
    } else if (str == "action_path") {
        m_actionPath = val;
        return true;
    } else if (str == "group_path") {
        m_usersGroupsPath = val;
        return true;
    } else {
        std::cerr << "ERROR the specified " << str << " config is unknown" << std::endl;
        return false;
    }
    return false;
}

