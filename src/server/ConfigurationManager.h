#pragma once

#include <iostream>
#include <string>
#include <stdexcept>

#include <utility/Macros.h>
#include <utility/SingletonBase.h>
#include <utility/Spin.h>

typedef unsigned short Port;

namespace gix {
namespace server {

class ConfigurationManager SINGLETON_BASE(Spin)
{
    SINGLETON_DECL(ConfigurationManager)

public:
    inline const std::string& getUserDataPath() const
    {
        return m_userDataPath;
    }

    inline const std::string& getActionPath() const
    {
        return m_actionPath;
    }

    inline Port getPort() const
    {
        return m_port;
    }

    inline const std::string& getUsersGroupsPath() const
    {
        return m_usersGroupsPath;
    }

    bool report(const std::string& str) const;
    bool set(const std::string& str, const std::string& val);

private:
    Port               m_port;
    const std::string  m_configPath;
    std::string        m_userDataPath;
    std::string        m_actionPath;
    std::string        m_usersGroupsPath;
};

} //namespace server
} // namespace gix
