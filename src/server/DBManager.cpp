#include <cstring>
#include <stdlib.h>

#include <server/ConfigurationManager.h>
#include <server/DBManager.h>

using namespace gix::server;
using namespace gix::generic;

SINGLETON_DEF(DBManager)

bool DBManager::authenticate(const std::string& username, const std::string& password)
{
    m_userData.load();
    const auto result = Hasher::md5Shortened(password + username) == getPasswordHash(username);
    m_userData.close();
    return result;
}

bool DBManager::usernameExists(const std::string& username)
{
    m_userData.load();
    const auto result = !getPasswordHash(username).empty();
    m_userData.close();
    return result;
}

void DBManager::addUser(const std::string& username, const std::string& password)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    const auto newLine = Hasher::md5Shortened(username) + Hasher::md5Shortened(password + username);
    FileLoader outFileLoader("tmp", std::ios_base::out);
    m_userData.load();
    outFileLoader.load();
    auto newLineWritten = false;
    Hash128 h;
    auto currentPosition = 0;
    const auto end  = m_userData.size();
    while (currentPosition < end) {
        h = m_userData.read(currentPosition, s_userDataLineSize);
        currentPosition += s_userDataLineSize;
        if (!newLineWritten && h > newLine) {
            outFileLoader.write(newLine);
            newLineWritten = true;
        }
        outFileLoader.write(h);
    }
    if (!newLineWritten) {
        outFileLoader.write(newLine);
    }
    m_userData.close();
    outFileLoader.close();
    ::system((std::string("mv tmp ") + m_userData.filename()).c_str());
}

gix::generic::Hash128 DBManager::getPasswordHash(const std::string& username)
{
    m_userData.load();
    const auto username_hash = gix::generic::Hasher::md5Shortened(username);
    const auto line_count = m_userData.size() / s_userDataLineSize;
    auto interval_start = 0;
    auto interval_end = line_count;
    while (interval_start < interval_end) {
        const auto current_line = (interval_start + interval_end) / 2;
        auto current_username_hash = m_userData.read(current_line * s_userDataLineSize, s_usernameHashSize);
        const auto c = std::strcmp(reinterpret_cast<const char*>(username_hash.c_str()),
                                   reinterpret_cast<const char*>(current_username_hash.c_str()));
        if (c == 0) {
            return m_userData.read(current_line * s_userDataLineSize + s_usernameHashSize, s_passwordHashSize);
        } else if (c < 0) {
            interval_end = current_line;
        } else {
            interval_start = current_line + 1;
        }
    }
    return Hash128();
}

DBManager::DBManager()
    : m_userData(ConfigurationManager::get_instance()->getUserDataPath(), std::ios_base::in)
{
}

DBManager::~DBManager()
{
}

