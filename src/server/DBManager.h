#pragma once

#include <string>
#include <vector>
#include <mutex>

#include <generic/Hasher.h>
#include <server/FileLoader.h>
#include <utility/SingletonBase.h>
#include <utility/Macros.h>

namespace gix {
namespace server {

class DBManager SINGLETON_BASE(std::mutex)
{
    SINGLETON_DECL(DBManager)

public:
    bool authenticate(const std::string& username, const std::string& password);
    bool usernameExists(const std::string& username);
    void addUser(const std::string& username, const std::string& password);

private:
    gix::generic::Hash128 getPasswordHash(const std::string& username);

private:
    FileLoader m_userData;

private:
    static const int32_t s_usernameHashSize = 16;
    static const int32_t s_passwordHashSize = 16;
    static const int32_t s_userDataLineSize = s_usernameHashSize + s_passwordHashSize;
};

} // namespace server
} // namespace gix
