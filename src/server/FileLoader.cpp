#include <server/FileLoader.h>

using namespace gix::server;

FileLoader::FileLoader(const std::string& filename, const Mode mode)
    : m_refCount(0)
    , m_filename(filename)
    , m_mode(mode)
    , m_size(0)
{}

FileLoader::~FileLoader()
{
    m_stream.close();
}

void FileLoader::load()
{
    if (m_refCount++ == 0) {
        m_stream.open(m_filename, m_mode | std::ios::ate | std::ios::binary);
	if (m_mode & std::ios::out) {
		m_size = m_stream.tellp();
		m_stream.seekp(0);
	} else if (m_mode & std::ios::in) {
		m_size = m_stream.tellg();
		m_stream.seekg(0);
	}
    }
}

void FileLoader::close()
{
    if (--m_refCount == 0) {
        m_stream.close();
    }
}

FileLoader::Data FileLoader::read(const std::fstream::pos_type pos, const uint32_t size)
{
    Data result(size, 0);
    if (m_mode & std::ios::out) {
	    m_stream.seekp(pos);
    } else if (m_mode & std::ios::in) {
	    m_stream.seekg(pos);
    }
    m_stream.read(reinterpret_cast<char*>(const_cast<unsigned char*>(result.data())), size);
    return result;
}

FileLoader::Data FileLoader::read(const std::fstream::pos_type pos /*= 0*/)
{
    return read(pos, m_size);
}

void FileLoader::write(const Data& data)
{
    m_stream.write(reinterpret_cast<const char*>(data.data()), data.size());
}
