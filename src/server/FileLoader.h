#pragma once

#include <string>
#include <fstream>

namespace gix {
namespace server {

class FileLoader
{
public:
    using Mode = std::ios_base::openmode;
    using Data = std::basic_string<uint8_t>;

public:
    FileLoader(const std::string& filename, const Mode mode);
    FileLoader(const FileLoader&) = delete;
    FileLoader(FileLoader&&) = delete;
    FileLoader& operator=(const FileLoader&) = delete;
    FileLoader& operator=(FileLoader&&) = delete;
    ~FileLoader();

    inline std::string filename() const
    {
        return m_filename;
    }

    inline int size() const
    {
        return m_size;
    }

    void load();
    void close();
    Data read(const std::fstream::pos_type pos, const uint32_t size);
    Data read(const std::fstream::pos_type pos = 0);
    void write(const Data& data);

private:
    std::fstream        m_stream;
    uint32_t            m_refCount;
    const std::string   m_filename;
    const Mode          m_mode;
    int32_t             m_size;
};

} // namespace server
} // namespace gix
