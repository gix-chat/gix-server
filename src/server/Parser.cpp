#include <mutex>

#include <server/Parser.h>

using namespace gix::parser;

namespace {

    const std::string whitespace = std::string(" \t");

} //unnamed namespace

SINGLETON_DEF(Parser)

Parser::Parser()
{
}

Parser::~Parser()
{
}

void Parser::setValue(const std::string& value)
{
    std::lock_guard<Spin> lock(m_mutex);
    m_value = value;
}

std::string Parser::getCurrentValue(const std::string& val) const
{
    size_t pos = m_value.find(val);
    if (pos != std::string::npos) {
        size_t startPos = m_value.find_first_not_of(whitespace, pos + val.size());
        if (startPos != std::string::npos) {
            size_t endPos = m_value.find_first_of(whitespace, startPos);
            return m_value.substr(startPos, endPos - startPos);
        }
    }
    return std::string();
}
