#pragma once

#include <string>

#include <utility/Macros.h>
#include <utility/SingletonBase.h>
#include <utility/Spin.h>

namespace gix {

namespace parser {

class Parser SINGLETON_BASE(Spin)
{
    SINGLETON_DECL(Parser)
        
public:
    void setValue(const std::string& value);
    std::string getCurrentValue(const std::string& val) const;

private:
    std::string m_value;        
};

} //namespace parser

} //namespace gix
