#include <csignal>
#include <exception>
#include <iostream>

#include <server/ActionsManager.h>
#include <server/ConfigurationManager.h>
#include <server/DBManager.h>
#include <server/UserManager.h>
#include <server/SignalHandler.h>
#include <utility/SingletonBase.h>

void gix::server::normalExit()
{
    SingletonStack::remove_instance();
    std::exit(0);
}

void gix::server::handler(int signal)
{
    if (signal == SIGTERM || signal == SIGABRT) {
        std::signal(SIGTERM, SIG_IGN);
        std::signal(SIGABRT, SIG_IGN);
    }
    std::cerr << signal << " signal was handled." << std::endl;
    normalExit();
}

void gix::server::setSignalHandlers()
{
    std::signal(SIGABRT, handler);
    std::signal(SIGFPE , handler);
    std::signal(SIGILL , handler);
    std::signal(SIGINT , handler);
    std::signal(SIGSEGV, handler);
    std::signal(SIGTERM, handler);
}
