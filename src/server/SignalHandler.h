#pragma once

namespace gix {
namespace server {

void normalExit();

void handler(int signal);

void setSignalHandlers();

} // namespace server
} // namespace gix
