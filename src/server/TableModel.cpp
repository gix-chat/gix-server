#include <string>
#include <iterator>
#include <cassert>

#include <server/TableModel.h>


TableModelBase::TableModelBase(bool hasRowNames, bool hasColumnNames)
    : m_hasRowNames(hasRowNames)
    , m_hasColumnNames(hasColumnNames)
{
}

const std::string& TableModelBase::getTitle() const
{
    return std::move(std::string());
}

bool TableModelBase::hasRowName() const
{
    return m_hasRowNames;
}

const std::string& TableModelBase::getRowName(uint8_t idx) const
{
    return std::move(std::string());
}

bool TableModelBase::hasColumnName() const
{
    return m_hasColumnNames;
}

const std::string& TableModelBase::getColumnName(uint8_t idx) const
{
    return std::move(std::string());
}

const std::string& TableModelBase::getCellData(uint8_t row, uint8_t column) const
{
    return std::move(std::string());
}

//!------------------------TableModel----------------------
TableModel::TableModel(const std::string& title,
                        const std::vector<std::string>& rowNames,
                        const std::vector<std::string>& columnNames, 
                        std::vector<std::vector<std::string> >& tableValues)
    : TableModelBase(rowNames.size() > 0, columnNames.size() > 0)
    , m_title(title)
    , m_rowNames(rowNames)
    , m_columnNames(columnNames)
    , m_values(tableValues)
{
}

TableModel::TableModel(const std::vector<std::string>& rowNames,
                        const std::vector<std::string>& columnNames,
                        std::vector<std::vector<std::string> >& tableValues)
    : TableModelBase(rowNames.size() > 0, columnNames.size() > 0)
    , m_title(std::string())
    , m_rowNames(rowNames)
    , m_columnNames(columnNames)
    , m_values(tableValues)
{
}

TableModel::TableModel(const TableModel& other)
    : TableModelBase(other.hasRowName(), other.hasColumnName())
    , m_title(other.m_title)
    , m_rowNames(other.m_rowNames)
    , m_columnNames(other.m_columnNames)
    , m_values(other.m_values)
{
}

//TODO check it first
TableModel& TableModel::operator= (const TableModel& other)
{
    if (this != &other) {
        m_hasRowNames = other.m_hasRowNames;
        m_hasColumnNames = other.m_hasColumnNames;
        m_title = other.m_title;
        m_rowNames = other.m_rowNames;
        m_columnNames = other.m_columnNames;
        m_values = other.m_values;
    }
    return *this;
}

const std::string& TableModel::getTitle() const
{
    return m_title;
}

bool TableModel::hasRowName() const
{
    return hasRowName();
}
    
const std::string& TableModel::getRowName(const uint8_t idx) const
{
   assert(idx < m_rowNames.size());
   return m_rowNames[idx];
}

bool TableModel::hasColumnName() const
{
    return hasColumnName();
}

const std::string& TableModel::getColumnName(uint8_t idx) const
{
    assert(idx < m_columnNames.size());
    return m_columnNames[idx];
}

const std::string& TableModel::getCellData(uint8_t row, uint8_t column) const
{
    return m_values[row][column];
}
uint8_t TableModel::getRowCount() const
{
    return m_rowNames.size();
}

uint8_t TableModel::getColumnCount() const
{
    return m_columnNames.size();
}
