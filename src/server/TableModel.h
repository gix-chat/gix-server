#pragma once

#include <list>
#include <vector>

class TableModelBase
{
public:
    /// @brief constructor
    TableModelBase(bool hasRowName = false, bool hasColumnName = false);

    /// @brief destructor
    virtual ~TableModelBase() = default;

    /// @brief returns the table name
    virtual const std::string& getTitle() const;

    /// @brief returns true if table has assotiate row name otherwise false
    virtual bool hasRowName() const;
    
    /// @brief returns the specified row name
    virtual const std::string& getRowName(const uint8_t idx) const;

    /// @brief returns true if table has assotiate column name otherwise false
    virtual bool hasColumnName() const;

    /// @brief returns the specified column name
    virtual const std::string& getColumnName(uint8_t idx) const; 

    /// @brief returns the specified cell data
    virtual const std::string& getCellData(uint8_t row, uint8_t column) const;

    /// @brief returns the total row count
    virtual uint8_t getRowCount() const = 0;

    /// @brief returns the total column count
    virtual uint8_t getColumnCount() const = 0;

protected:
    bool m_hasRowNames;
    bool m_hasColumnNames;
};

class TableModel : public TableModelBase
{
public:
    TableModel(const std::string& title, const std::vector<std::string>& rowNames,
                const std::vector<std::string>& columnNames, std::vector<std::vector<std::string> >& tableValues);

    TableModel(const std::vector<std::string>& rowNames, const std::vector<std::string>& columnNames,
                std::vector<std::vector<std::string> >& tableValues);

    //!TODO need to write same type generic constructor to get table values from any type container

    TableModel(const TableModel& other);
    TableModel& operator= (const TableModel& other);

    virtual ~TableModel() = default;


    /// @brief returns the table name
    virtual const std::string& getTitle() const override;

    /// @brief returns true if table has assotiate row name otherwise false
    virtual bool hasRowName() const override;
    
    /// @brief returns the specified row name
    virtual const std::string& getRowName(const uint8_t idx) const override;

    /// @brief returns true if table has assotiate column name otherwise false
    virtual bool hasColumnName() const override;

    /// @brief returns the specified column name
    virtual const std::string& getColumnName(uint8_t idx) const override; 

    /// @brief returns the specified cell data
    virtual const std::string& getCellData(uint8_t row, uint8_t column) const override;

    /// @brief returns the total row count
    virtual uint8_t getRowCount() const override;

    /// @brief returns the total column count
    virtual uint8_t getColumnCount() const override;

private:
    std::string m_title;
    std::vector<std::string> m_rowNames;
    std::vector<std::string> m_columnNames;
    std::vector<std::vector<std::string> > m_values;

};
