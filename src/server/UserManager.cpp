#include <mutex>

#include <server/ConfigurationManager.h>
#include <server/FileLoader.h>
#include <server/GlobalSerializers.h>
#include <server/UserManager.h>

using namespace gix::server;

SINGLETON_DEF(UserManager)

UserManager::UserManager()
    : m_isCurrentStateSaved(true)
{
}

UserManager::~UserManager()
{
    save();
}

void UserManager::save() const
{
    if (m_isCurrentStateSaved) {
        return;
    }
    m_isCurrentStateSaved = true;
    auto data = toByteArray(m_usernames) + toByteArray(m_groups);
    FileLoader outFileLoader(ConfigurationManager::get_instance()->getUsersGroupsPath(), std::ios::out);
    outFileLoader.load();
    outFileLoader.write(data);
}

void UserManager::load()
{
    std::lock_guard<Spin> lock(m_spin);
    m_isCurrentStateSaved = false;
    clear();
    ByteArray data;
    {
        FileLoader inputFileLoader(ConfigurationManager::get_instance()->getUsersGroupsPath(), std::ios::in);
        inputFileLoader.load();
        data = inputFileLoader.read();
    }
    int index = 0;
    index += fromByteArray(&data[index], m_usernames);
    index += fromByteArray(&data[index], m_groups);
}

bool UserManager::userExists(const std::string& user) const
{
    return m_usernames.find(user) != m_usernames.end();
}

const UserManager::Users& UserManager::getUsers(const std::string& group/*= std::string()*/) const
{
    auto it = m_groups.find(group);
    if (it == m_groups.end()) {
        return m_usernames;
    }
    return it->second;
}

const UserManager::Groups& UserManager::getGroups() const
{
    return m_groups;
}

void UserManager::addUser(const std::string& user)
{
    std::lock_guard<Spin> lock(m_mutex);
    m_isCurrentStateSaved = false;
    m_usernames.insert(user);
}

void UserManager::addGroup(const std::string& group)
{
    std::lock_guard<Spin> lock(m_mutex);
    m_isCurrentStateSaved = false;
    m_groups.insert(std::make_pair(group, Users()));
}

void UserManager::addUserToGroup(const std::string& user, const std::string& group)
{
    std::lock_guard<Spin> lock(m_mutex);
    m_isCurrentStateSaved = false;
    auto it = m_groups.find(group);
    if ((it != m_groups.end()) && (m_usernames.find(user) != m_usernames.end())) {
        it->second.insert(user);
    }
}

void UserManager::clear()
{
    std::lock_guard<Spin> lock(m_mutex);
    m_usernames.clear();
    m_groups.clear();
}
