#pragma once

#include <map>
#include <set>
#include <string>

#include <utility/Macros.h>
#include <utility/SingletonBase.h>
#include <utility/Spin.h>

namespace gix {
namespace server {

class UserManager SINGLETON_BASE(Spin)
{
    SINGLETON_DECL(UserManager)

private:
    using Users = std::set<std::string>;
    using Groups = std::map<std::string, Users>;

public:
    void save() const;
    void load();
    bool userExists(const std::string& user) const;
    const Users& getUsers(const std::string& group = std::string()) const;
    const Groups& getGroups() const;
    void addUser(const std::string& user);
    void addGroup(const std::string& group);
    void addUserToGroup(const std::string& user, const std::string& group);

private:
    void clear();

private:
    Users           m_usernames;
    Groups          m_groups;
    mutable bool    m_isCurrentStateSaved;
    Spin            m_spin;
};

} // namespace server

} // namespace gix
