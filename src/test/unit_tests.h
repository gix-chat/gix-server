#pragma once

#include <iostream>

#include <server/Actions.h>
#include <server/ActionsManager.h>
#include <server/ConfigurationManager.h>
#include <server/DBManager.h>
#include <server/UserManager.h>

using namespace gix::server;

void testUserData(const bool verbose = false)
{
#ifdef __linux__
    ::system((std::string("rm ") + ConfigurationManager::get_instance()->getUserDataPath() + std::string(" > /dev/null 2>&1&")).c_str());
#elif
    // TODO!
#endif

    if (verbose) {
        std::cout << "Starting User data test." << std::endl;
    }

    auto mgr = gix::server::DBManager::get_instance();

    mgr->addUser("user1", "pass1");
    mgr->addUser("user2", "pass2");
    mgr->addUser("user3", "pass3");
    mgr->addUser("user4", "pass4");
    mgr->addUser("user5", "pass5");
    mgr->addUser("user6", "pass6");
    mgr->addUser("user7", "pass7");
    mgr->addUser("user8", "pass8");
    mgr->addUser("user9", "pass9");
    mgr->addUser("user10", "pass10");
    mgr->addUser("user11", "pass11");
    mgr->addUser("user12", "pass12");
    mgr->addUser("user13", "pass13");
    mgr->addUser("user14", "pass14");
    mgr->addUser("user15", "pass15");
    mgr->addUser("user16", "pass16");
    mgr->addUser("user17", "pass17");
    mgr->addUser("user18", "pass18");
    mgr->addUser("user19", "pass19");
    mgr->addUser("user20", "pass20");

    bool pass = true;
    if (!mgr->usernameExists("user1") ||
        !mgr->usernameExists("user5") ||
        !mgr->usernameExists("user10") ||
        !mgr->usernameExists("user15") ||
        !mgr->usernameExists("user20") ||
        mgr->usernameExists("user21")) {
        pass = false;
        if (verbose) {
            std::cout << "Failed on usernameExists()." << std::endl;
        }
    }
    if (!mgr->authenticate("user1", "pass1") ||
        !mgr->authenticate("user17", "pass17") ||
        !mgr->authenticate("user20", "pass20") ||
        mgr->authenticate("user11", "pass12") ||
        mgr->authenticate("user20", "pass12") ||
        mgr->authenticate("user", "pass")) {
        pass = false;
        if (verbose) {
            std::cout << "Failed on authenticate()." << std::endl;
        }
    }
    if (verbose) {
        std::cout << "User data test finished." << std::endl;
    }
    std::cout << "User data test " << (pass ? "passed" : "failed") << '.' << std::endl;

#ifdef __linux__
    ::system((std::string("rm ") + ConfigurationManager::get_instance()->getUserDataPath() + std::string(" > /dev/null 2>&1&")).c_str());
#elif
    // TODO!
#endif
}

void testActionsManager( const bool verbose = false)
{
#ifdef __linux__
    ::system((std::string("rm ") + ConfigurationManager::get_instance()->getActionPath() + std::string(" > /dev/null 2>&1&")).c_str());
#elif
    // TODO!
#endif
    bool pass = true;
    if (verbose) {
        std::cout << "Starting ActionsManager test." << std::endl;
    }

    auto mgr = ActionsManager::get_instance();

    SendPersonalMessageAction* spmAct = new SendPersonalMessageAction();
    spmAct->m_senderUsername = "sender";
    spmAct->m_receiverUsername = "receiver";
    spmAct->m_message = "message";
    mgr->addAction(spmAct->clone());

    SendGroupMessageAction* sgmAct = new SendGroupMessageAction();
    sgmAct->m_senderUsername = "sender";
    sgmAct->m_groupName = "group";
    sgmAct->m_message = "message";
    sgmAct->m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
    mgr->addAction(sgmAct->clone());

    JoinGroupAction* jgAct = new JoinGroupAction();
    jgAct->m_joiningUsername = "joining";
    jgAct->m_groupOwner = "owner";
    jgAct->m_groupName = "group";
    mgr->addAction(jgAct->clone());

    JoinGroupAcceptedAction* jgaAct = new JoinGroupAcceptedAction();
    jgaAct->m_groupName = "group";
    jgaAct->m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
    mgr->addAction(jgaAct->clone());

    LeftGroupAction* lgAct = new LeftGroupAction();
    lgAct->m_groupName = "group";
    lgAct->m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
    mgr->addAction(lgAct->clone());

    RemoveFromGroupAction* rfgAct = new RemoveFromGroupAction();
    rfgAct->m_removedUsername = "removed";
    rfgAct->m_groupName = "group";
    rfgAct->m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
    mgr->addAction(rfgAct->clone());

    mgr->save();
    mgr->load();

    auto actions = mgr->getActions();
    for (auto act : actions) {
        auto spmtmp = dynamic_cast<SendPersonalMessageAction*>(act);
        auto sgmtmp = dynamic_cast<SendGroupMessageAction*>(act);
        auto jgtmp = dynamic_cast<JoinGroupAction*>(act);
        auto jgatmp = dynamic_cast<JoinGroupAcceptedAction*>(act);
        auto lgtmp = dynamic_cast<LeftGroupAction*>(act);
        auto rfgtmp = dynamic_cast<RemoveFromGroupAction*>(act);
        if (spmtmp != nullptr) {
            if (!(spmtmp->m_senderUsername == spmAct->m_senderUsername) ||
                !(spmtmp->m_receiverUsername == spmAct->m_receiverUsername) ||
                !(spmtmp->m_message == spmAct->m_message)) {
                pass = false;
                if (verbose) {
                    std::cout << "Failed on SendPersonalMessageAction." << std::endl;
                }
            }
        }
        if (sgmtmp != nullptr) {
            if (!(sgmtmp->m_senderUsername == sgmAct->m_senderUsername) ||
                !(sgmtmp->m_groupName == sgmAct->m_groupName) ||
                !(sgmtmp->m_receiversLeft == sgmAct->m_receiversLeft) ||
                !(sgmtmp->m_message == sgmAct->m_message)) {
                pass = false;
                if (verbose) {
                    std::cout << "Failed on SendGroupMessageAction." << std::endl;
                }
            }
        }
        if (jgtmp != nullptr) {
            if (!(jgtmp->m_joiningUsername == jgAct->m_joiningUsername) ||
                !(jgtmp->m_groupName == jgAct->m_groupName) ||
                !(jgtmp->m_groupOwner == jgAct->m_groupOwner)) {
                pass = false;
                if (verbose) {
                    std::cout << "Failed on JoinGroupAction." << std::endl;
                }
            }
        }
        if (jgatmp != nullptr) {
            if (!(jgatmp->m_receiversLeft == jgaAct->m_receiversLeft) ||
                !(jgatmp->m_groupName == jgaAct->m_groupName)) {
                pass = false;
                if (verbose) {
                    std::cout << "Failed on JoinGroupAcceptedAction." << std::endl;
                }
            }
        }
        if (lgtmp != nullptr) {
            if (!(lgtmp->m_groupName == lgAct->m_groupName) ||
                !(lgtmp->m_receiversLeft == lgAct->m_receiversLeft)) {
                pass = false;
                if (verbose) {
                    std::cout << "Failed on LeftGroupAction." << std::endl;
                }
            }
        }
        if (rfgtmp != nullptr) {
            if (!(rfgtmp->m_removedUsername == rfgAct->m_removedUsername) ||
                !(rfgtmp->m_groupName == rfgAct->m_groupName) ||
                !(rfgtmp->m_receiversLeft == rfgAct->m_receiversLeft)) {
                pass = false;
                if (verbose) {
                    std::cout << "Failed on RemoveFromGroupAction." << std::endl;
                }
            }
        }
    }

    delete spmAct;
    delete sgmAct;
    delete jgAct;
    delete jgaAct;
    delete lgAct;
    delete rfgAct;

    if (verbose) {
        std::cout << "ActionsManager test finished." << std::endl;
    }
    std::cout << "ActionsManager test " << (pass ? "passed" : "failed") << '.' << std::endl;
#ifdef __linux__
    ::system((std::string("rm ") + ConfigurationManager::get_instance()->getActionPath() + std::string(" > /dev/null 2>&1&")).c_str());
#elif
    // TODO!
#endif
}

void testActionSerialization(const bool verbose = false)
{
    bool pass = true;
    if (verbose) {
        std::cout << "Starting Action serialization test." << std::endl;
    }

    ByteArray data;
    {
        SendPersonalMessageAction spmAct;
        spmAct.m_senderUsername = "sender";
        spmAct.m_receiverUsername = "receiver";
        spmAct.m_message = "message";
        data += spmAct.serialize();

        SendGroupMessageAction sgmAct;
        sgmAct.m_senderUsername = "sender";
        sgmAct.m_groupName = "group";
        sgmAct.m_message = "message";
        sgmAct.m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
        data += sgmAct.serialize();

        JoinGroupAction jgAct;
        jgAct.m_joiningUsername = "joining";
        jgAct.m_groupOwner = "owner";
        jgAct.m_groupName = "group";
        data += jgAct.serialize();

        JoinGroupAcceptedAction jgaAct;
        jgaAct.m_groupName = "group";
        jgaAct.m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
        data += jgaAct.serialize();

        LeftGroupAction lgAct;
        lgAct.m_groupName = "group";
        lgAct.m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
        data += lgAct.serialize();

        RemoveFromGroupAction rfgAct;
        rfgAct.m_removedUsername = "removed";
        rfgAct.m_groupName = "group";
        rfgAct.m_receiversLeft = { "receiver1", "receiver2", "receiver3" };
        data += rfgAct.serialize();
    }
    {
        int index = 0;
        index += 4;
        SendPersonalMessageAction spmAct;
        index += spmAct.deserialize(&data[index]);
        if (!(spmAct.m_senderUsername == "sender") ||
            !(spmAct.m_receiverUsername == "receiver") ||
            !(spmAct.m_message == "message")) {
            pass = false;
            if (verbose) {
                std::cout << "Failed on SendPersonalMessageAction." << std::endl;
            }
        }

        SendGroupMessageAction sgmAct;
        index += 4;
        index += sgmAct.deserialize(&data[index]);
        if (!(sgmAct.m_senderUsername == "sender") ||
            !(sgmAct.m_groupName == "group") ||
            !(sgmAct.m_message == "message") ||
            !(sgmAct.m_receiversLeft == std::set<std::string>{ "receiver1", "receiver2", "receiver3" })) {
            pass = false;
            if (verbose) {
                std::cout << "Failed on SendGroupMessageAction." << std::endl;
            }
        }

        JoinGroupAction jgAct;
        index += 4;
        index += jgAct.deserialize(&data[index]);
        if (!(jgAct.m_joiningUsername == "joining") ||
            !(jgAct.m_groupOwner == "owner") ||
            !(jgAct.m_groupName == "group")) {
            pass = false;
            if (verbose) {
                std::cout << "Failed on JoinGroupAction." << std::endl;
            }
        }

        JoinGroupAcceptedAction jgaAct;
        index += 4;
        index += jgaAct.deserialize(&data[index]);
        if (!(jgaAct.m_groupName == "group") ||
            !(jgaAct.m_receiversLeft == std::set<std::string>{ "receiver1", "receiver2", "receiver3" })) {
            pass = false;
            if (verbose) {
                std::cout << "Failed on JoinGroupAcceptedAction." << std::endl;
            }
        }

        LeftGroupAction lgAct;
        index += 4;
        index += lgAct.deserialize(&data[index]);
        if (!(lgAct.m_groupName == "group") ||
            !(lgAct.m_receiversLeft == std::set<std::string>{ "receiver1", "receiver2", "receiver3" })) {
            pass = false;
            if (verbose) {
                std::cout << "Failed on LeftGroupAction." << std::endl;
            }
        }

        RemoveFromGroupAction rfgAct;
        index += 4;
        index += rfgAct.deserialize(&data[index]);
        if (!(rfgAct.m_removedUsername == "removed") ||
            !(rfgAct.m_groupName == "group") ||
            !(rfgAct.m_receiversLeft == std::set<std::string>{ "receiver1", "receiver2", "receiver3" })) {
            pass = false;
            if (verbose) {
                std::cout << "Failed on LeftGroupAction." << std::endl;
            }
        }
    }

    if (verbose) {
        std::cout << "Action serialization test finished." << std::endl;
    }
    std::cout << "Action serialization test " << (pass ? "passed" : "failed") << '.' << std::endl;
}


void testUserManager(const bool verbose = false)
{
#ifdef __linux__
    ::system((std::string("rm ") + ConfigurationManager::get_instance()->getUsersGroupsPath() + std::string(" > /dev/null 2>&1&")).c_str());
#elif
    // TODO!
#endif
    bool pass = true;
    if (verbose) {
        std::cout << "Starting UserManager test." << std::endl;
    }
    auto mgr = UserManager::get_instance();
    mgr->addUser("user1");
    mgr->addUser("user2");
    mgr->addUser("user3");
    mgr->addUser("user4");
    mgr->addUser("user5");
    mgr->addUser("user6");

    mgr->addGroup("group1");
    mgr->addGroup("group2");
    mgr->addGroup("group3");

    mgr->addUserToGroup("user1", "group1");
    mgr->addUserToGroup("user2", "group1");
    mgr->addUserToGroup("user5", "group1");

    mgr->addUserToGroup("user3", "group2");
    mgr->addUserToGroup("user4", "group2");
    mgr->addUserToGroup("user5", "group2");

    mgr->addUserToGroup("user1", "group3");
    mgr->addUserToGroup("user4", "group3");
    mgr->addUserToGroup("user6", "group3");

    const auto usersBefore = mgr->getUsers();
    const auto groupsBefore = mgr->getGroups();
    mgr->save();
    mgr->addUser("dummy"); // So that data changes and load will not be ignored
    mgr->load();

    const auto usersAfter = mgr->getUsers();
    const auto groupsAfter = mgr->getGroups();

    if (usersBefore != usersAfter) {
        pass = false;
        if (verbose) {
            std::cout << "Failed on users." << std::endl;
        }
    }

    if (groupsBefore != groupsAfter) {
        pass = false;
        if (verbose) {
            std::cout << "Failed on groups." << std::endl;
        }
    }

    if (verbose) {
        std::cout << "UserManager test finished." << std::endl;
    }
    std::cout << "UserManager test " << (pass ? "passed" : "failed") << '.' << std::endl;
#ifdef __linux__
    ::system((std::string("rm ") + ConfigurationManager::get_instance()->getUsersGroupsPath() + std::string(" > /dev/null 2>&1&")).c_str());
#elif
    // TODO!
#endif
}
