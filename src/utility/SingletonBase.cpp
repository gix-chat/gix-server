#include <utility/SingletonBase.h>

SINGLETON_DEF(SingletonStack)

SingletonStack::SingletonStack()
{
}

SingletonStack::~SingletonStack()
{
    while (!m_stack.empty()) {
        if (m_set.find(m_stack.top()) == m_set.end()) {
            m_set.insert(m_stack.top());
            delete m_stack.top();
        }
        m_stack.pop();
    }
}

void SingletonStack::push(SingletonInterface* obj)
{
    m_stack.push(obj);
    auto it = m_set.find(obj);
    if (it != m_set.end()) {
        m_set.erase(it);
    }
}

void SingletonStack::addRemoved(SingletonInterface* obj)
{
    m_set.insert(obj);
}
