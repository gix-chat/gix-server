#pragma once

#include <set>
#include <stack>

#include <utility/Macros.h>

class SingletonInterface
{
public:
    SingletonInterface(const SingletonInterface&) = delete;
    SingletonInterface(SingletonInterface&&) = delete;
    SingletonInterface& operator= (const SingletonInterface&) = delete;
    SingletonInterface& operator= (SingletonInterface&&) = delete;

    SingletonInterface()
    {}
};

template <typename T>
class SingletonBase : public SingletonInterface
{
public:
    SingletonBase();
    virtual ~SingletonBase();

    SingletonBase(const SingletonBase&) = delete;
    SingletonBase(SingletonBase&&) = delete;
    SingletonBase& operator=(const SingletonBase&) = delete;
    SingletonBase& operator=(SingletonBase&&) = delete;

protected:
    mutable T m_mutex;
};

class SingletonStack
{
    SINGLETON_DECL(SingletonStack)

public:
    void push(SingletonInterface* obj);
    void addRemoved(SingletonInterface* obj);

private:
    std::set<SingletonInterface*>    m_set;
    std::stack<SingletonInterface*>  m_stack;
};

#include "SingletonBase.hpp"
