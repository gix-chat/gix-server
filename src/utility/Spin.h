#pragma once

#include <atomic>
#include <thread>

class Spin
{
public:
    Spin()
        : m_flag(ATOMIC_FLAG_INIT)
    {}

    bool try_lock()
    {
        return !m_flag.test_and_set(std::memory_order_acquire);
    }

    void lock ()
    {
        while (m_flag.test_and_set(std::memory_order_acquire));
    }

    void unlock()
    {
        m_flag.clear();
    }

private:
    std::atomic_flag m_flag;
};

class RecursiveSpin
{
public:
    RecursiveSpin()
        : m_flag(ATOMIC_FLAG_INIT)
    {
        m_tid = std::thread::id();
        m_count = 0;
    }

    bool try_lock()
    {
        if (m_tid != std::thread::id() && m_tid != std::this_thread::get_id()) {
            return false;
        }
        lock();
        return true;
    }

    void lock()
    {
        std::thread::id tid = std::this_thread::get_id();
        if (m_tid == tid) {
            ++m_count;
            return;
        }
        while (m_flag.test_and_set(std::memory_order_acquire));
        m_tid = tid;
        m_count = 1;
    }

    void unlock()
    {
        if (--m_count == 0) {
            return;
        }
        m_tid = std::thread::id();
        m_flag.clear();
    }

    void unlock_all()
    {
        m_count = 0;
        m_tid = std::thread::id();
        m_flag.clear();
    }
private:
    std::atomic_flag m_flag;
    std::atomic<std::thread::id> m_tid;
    std::atomic<int> m_count;
};
